<?php
require_once('dbconnect.php');
if (!empty($_POST)) {
	$message = trim(strip_tags($_POST['notepad']));
	//получаем id пользователя
	$sql = 'SELECT id FROM users WHERE login = ?';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_SESSION['auth']]);
	$userId = $stmt->fetchColumn();
	//добавляем запись в таблицу user_messages
	$sql = 'INSERT users_messages SET message = ?, userId = ?';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$message, $userId]);
	$_SESSION['message'] = '<div>Новая запись успешно добавлена</div>';
}
include('head.php');
?>
<h1>Онлайн блокнотик</h1>
<?php if (isset($_SESSION['auth'])) { ?>
<?= $_SESSION['message']?>
<form action="" method="post">
	<textarea name="notepad" id="" cols="30" rows="10"></textarea>
	<input type="submit" name="Сохранить">
</form>
<?php } else { ?>
	<p>Чтобы получить доступ к блокноту необходимо войти или зарегистрироваться</p>
<?php } ?>
</div>
	
</body>
</html>

<!-- Реализовать онлайн блокнотик. Пользователь заходит, регистрируется.
После регистрации видит большой текстареа, в котором может оставлять
какие-то записи. Записи будут сохранятся и выводится списком рядом
с текстареа.

при регистрации пользователь указывает свою страну, город, дату рождения

Дополнительные фичи:
Над текстареа сделать блоки (назовем их виджеты), в которых будет показываться:
курсы валют на сегодня, календарь с текущим месяцем, гороскоп на текущий день,
погода по городу пользователя, свежий анекдот. Все это (кроме календарика) должно
парсится с каких нибудь сайтов. -->