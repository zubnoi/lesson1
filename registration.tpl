<h1>Регистрация в блокнотике</h1>
<?= $_SESSION['message']; ?>
<form action="" method="post" class="reg">
	<div class="form-group">
		<label for="login">Введите логин</label><br>
		<input type="text" id="login" name="login" value="<?= $login ?>" required>			
	</div>
	<div class="form-group">
		<label for="password">Введите пароль</label><br>
		<input type="password" id="password" name="password" required>			
	</div>
	<div class="form-group">
		<label for="email">Введите email</label><br>
		<input type="email" id="email" name="email" value="<?= $email ?>" required>			
	</div>
	<div class="form-group">
		<label for="date">Введите дату рождения</label><br>
		<input type="date" id="date" name="date" value="<?= $birthday ?>" required>			
	</div>
	<div class="form-group">
		<label for="country">Введите свою страну</label><br>
		<input type="text" id="country" name="country" value="<?= $country ?>" required>			
	</div>
	<div class="form-group">
		<label for="city">Введите свой город</label><br>
		<input type="text" id="city" name="city" value="<?= $city ?>" required>			
	</div>
	<div class="form-group">
		<input type="submit" name="submit">			
	</div>
</form>
</div>	
</body>
</html>