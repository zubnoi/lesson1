<?php
require_once('queris.php');

$host = 'localhost';
$user = 'root';
$password = '';
$charset = 'utf-8';
$dsn = "mysql:host->$host;charset->$charset;";
$opt = [
	PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
	PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
	PDO::ATTR_EMULATE_PREPARES => false
];
$pdo = new PDO($dsn, $user, $password, $opt);
$pdo->exec(SQL_CREATE_DATABASE);
$pdo->exec('use lesson1');
$pdo->exec(SQL_CREATE_TABLE_USERS);
$pdo->exec(SQL_CREATE_TABLE_COUNTRIES);
$pdo->exec(SQL_CREATE_TABLE_CITIES);
$pdo->exec(SQL_CREATE_TABLE_USERS_MESSAGES);

session_start();