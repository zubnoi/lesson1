<?php
require_once('dbconnect.php');
//инициализируе переменные для values инпутов
$login = '';
$email = '';
$birthday = '';
$country = '';
$city = '';
if (!empty($_POST)) {
	//получаем данные из формы
	$login = trim(strip_tags($_POST['login']));
	$password = trim(strip_tags($_POST['password']));
	$email = trim(strip_tags($_POST['email']));
	$birthday = $_POST['date'];
	$country = trim(strip_tags($_POST['country']));
	$city = trim(strip_tags($_POST['city']));

	//делаем запрос в бд на проверку занятости логина
	$sql = 'SELECT COUNT(*) as count FROM users WHERE login = ?';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$login]);

	if (!($stmt->fetchColumn()))	{
		$sql = 'SELECT COUNT(*) as count FROM users WHERE email = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->execute([$email]);

		if (!($stmt->fetchColumn())) {
			//проверяем наличие страны и получаем ее id
			$sql = 'SELECT * FROM countries WHERE country = ?';
			$stmt = $pdo->prepare($sql);
			$stmt->execute([$country]);
			$row = $stmt->fetch(PDO::FETCH_LAZY);

			if ($row->id) {
				$countryId = $row->id;

			} else {
				$sql = 'INSERT INTO countries SET country = ?';
				$stmt = $pdo->prepare($sql);
				$stmt->execute([$country]);
				$countryId = $pdo->lastInsertId();
			}
			//проверяем наличие города и получаем его id
			$sql = 'SELECT * FROM cities WHERE city = ?';
			$stmt = $pdo->prepare($sql);
			$stmt->execute([$city]);
			$row = $stmt->fetch(PDO::FETCH_LAZY);

			if ($row->id) {
				$cityId = $row->id;
			} else {
				$sql = 'INSERT INTO cities SET city = ?';
				$stmt = $pdo->prepare($sql);
				$stmt->execute([$city]);
				$cityId = $pdo->lastInsertId();
			}
			//хешируем пароль
			$password = password_hash($password, PASSWORD_DEFAULT);
			//добавляем запись о пользователе
			$sql = 'INSERT INTO users (login, password, email, birthday, countryId, cityId) VALUES (:login, :password, :email, :birthday, :countryId, :cityId)';
			$stmt = $pdo->prepare($sql);
			$stmt->execute([
				':login' => $login,
				':password' => $password,
				':email' => $email,
				':birthday' => $birthday,
				':countryId' => $countryId,
				':cityId' => $cityId
			]);

			$_SESSION['auth'] = $login;
			header('Location: /lesson1/');
		}else {
			$_SESSION['message'] = '<div class="alert">Пользователь с таким email уже существует, воспользуйтесь другим почтовым ящиком</div>';
		}
	} else {
		$_SESSION['message'] = '<div class="alert">Этот логин занят, придумайте другой</div>';
	}
}
include('head.php');
include('registration.tpl');




