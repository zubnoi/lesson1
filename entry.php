<?php
require_once('dbconnect.php');
//инициализируе переменные для values инпутов
$login = '';
if (!empty($_POST)) {
	//получаем данные из формы
	$login = trim(strip_tags($_POST['login']));
	$password = trim(strip_tags($_POST['password']));

	//делаем запрос в бд на проверку занятости логина
	$sql = 'SELECT * FROM users WHERE login = ?';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$login]);
	$user = $stmt->fetch(PDO::FETCH_LAZY);

	if ($user->login)	{
		$passwordHash = $user->password;
		if (password_verify($password, $passwordHash)) {
			$_SESSION['auth'] = $user->login;
			header('Location: /lesson1/');
		} else {
			$_SESSION['message'] = '<div class="alert">Пользователя с таким логином или паролем не существует</div>';
		}
	} else {
		$_SESSION['message'] = '<div class="alert">Пользователя с таким логином или паролем не существует</div>';
	}
}
include('head.php');
include('entry.tpl');
