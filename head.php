<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Онлайн-блокнотик</title>
	<style>
		#wrapper{
			width: 1100px;
			margin: 0 auto;
			font-family:roboto;
		}
		header{
			display: flex;
			justify-content: flex-end;
		}
		header a{
			margin-left:12px;
			text-decoration: none;
		}
		form{
			width: 50%;
		}
		.reg{
			margin: 36px auto;
		    padding: 24px;
		    background: #ff98004d;
		    width: 550px;
		    border-radius: 36px;
		    text-align: center;
		}
		label{
			font-size: 20px;
    		font-family: roboto;
		}
		input{
			width: 500px;
		    padding: 16px;
		    border-radius: 36px;
		    outline: none;
		    font-size: 20px;
		    margin: 12px;
		    color: #2f2f2f;
		    border: none;
		}
		input[type="submit"]{
			width:530px;
		}
		input[type="submit"]:hover{
			background-color: red;
			cursor: pointer;
			color: #fff;
		}
		input[type="submit"]:active{
			transform: translateY(4px);
		}
		header span {
			margin-left:12px;

		}
		textarea{
			resize: none;
			width: 100%;
   			min-height: 400px;
   			overflow: auto;
		    border-radius: 5px;
		    padding: 12px 24px;
		}


	</style>
</head>
<body>
<div id="wrapper">
	<header>
		<a href="/lesson1">Главная</a>
<?php
	if (isset($_SESSION['auth'])) {
?>
		<span><?= $_SESSION['auth'] ?></span>
		<a href="/lesson1/logout.php">Выйти</a>
<?php
} else {
?>
		<a href="/lesson1/entry.php">Войти</a>
		<a href="/lesson1/registration.php">Зарегистрироваться</a>
		
<?php
}	
?>
	</header>